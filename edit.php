<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Bakery</title>
    <style>
        .myDiv {
            margin: auto;
            display: flex;
            justify-content: center;
        }
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body style="background-image: url('https://www.arlafoodsingredients.com/492f6a/globalassets/afi/industry/bakery/bakery-hero.jpg');">

<h1 style="text-align:center;font-size:500%;"> Bakery  </h1>
   <?php
       require_once 'connect.php';
       
       $id = $_GET['edit'];
       $results = mysqli_query($mysqli, "SELECT * FROM bakery WHERE ID =$id");
        $row= mysqli_fetch_array($results);
        $ID =$row['ID'];
        $Name =$row['Name'];
        $Price =$row['Price'];
        $Description =$row['Description'];
     ?>

    <div>
         <div class="myDiv">

            <div class=" row justify-content center">
                <form action="connect.php" method="POST" class="w-50">
                   <input type="hidden" name="ID" value="<?php echo $ID ; ?>">
                    <div>
                          <lable>Name</lable>
                          <input type="text" name="Name" class=" form-control" 
                           value="<?php echo $Name ; ?>">
                   </div>
                   <div>
                           <lable>Price</lable> 
                           <input type="number" name="Price" class=" form-control" 
                           value="<?php echo $Price ; ?>">
                   </div>
                    <div>
                           <lable>Descreption</lable>
                           <input type="text" name="Description"class=" form-control" 
                           value="<?php echo $Description; ?>">
                   </div>
                    <br>
                    <button type ="submit" name="update"> Update</button>
            
            

                 </form>
            </div>
        </div>
    </div>
     
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
        
</body>
</html>