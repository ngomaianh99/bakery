<?php require_once 'connect.php';?>


<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
      <title>Bakery</title>
      <style>
        .myDiv {
            margin: auto;
            display: flex;
            justify-content: center;
        }
      </style>

       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <body style="background-image: url('https://www.arlafoodsingredients.com/492f6a/globalassets/afi/industry/bakery/bakery-hero.jpg');">

     <h1 style="text-align:center;font-size:500%;"> Bakery  </h1>

      <?php 

          

          $result = $mysqli->query("SELECT*FROM bakery") or die($mysqli->error);
 
      ?>
    <div>
      <div class="myDiv">

        <div class=" row justify-content center">
          <form action="connect.php" method="POST" class="w-50">
           
            <div>
                <lable>Name</lable>
                <input type="text" name="Name" class=" input-form" 
                  value="<?php echo $Name ; ?>">
             </div>

             <div>
                 <lable>Price</lable> 
                 <input type="number" name="Price" class=" input-form" 
                  value="<?php echo $Price ; ?>">
             </div>

             <div>
                 <lable>Description</lable>
                 <input type="text" name="Description"class=" input-form" 
                   value="<?php echo $Description; ?>">
             </div>
             <br>
             <button class="btn btn-primary" type ="submit"name="Save"> Save</button>
           </form>
        </div>
      </div>
    </div>

      <br>
      
        
     <div class="container">
          <form action="search2.php" method="POST">   
               <button class="btn btn-primary" type="submit" name = "submit-search">search</button>
               <input style="width:90%"  class= "input-form" type="text" name = "search" >
           </form>
     </div>
          
      <br>

     <div class="container">
     <?php // page
      // tim tong records
       $result = $mysqli->query("SELECT count(ID) as total FROM bakery");
       $row = mysqli_fetch_assoc($result);
       $total_records = $row['total'];
      // tim limit va current page
       $current_page = isset($_GET['page']) ? $_GET['page'] :1;
       $limit = 3;
      // tinh total page
       $total_page = ceil($total_records / $limit) ;

       if ($current_page > $total_page){
            $current_page = $total_page;
       }
       else if ($current_page <1 ){
                $current_page = 1;
       }
       //tim start
       $start = ( $current_page -1) * $limit;
       
       $result = $mysqli->query("SELECT * FROM bakery LIMIT $start, $limit");
       
       // phan trang
        if ($current_page > 1 && $total_page > 1){
        echo '<a href="1234.php?page='.($current_page-1).'">Prev</a> | ';
        }

        for ($i = 1; $i <= $total_page; $i++){
         
          if ($i == $current_page){
            echo '<span>'.$i.'</span> | ';
          }
    
           else{
            echo '<a href="1234.php?page='.$i.'">'.$i.'</a> | ';
           }
        } 
        if ($current_page < $total_page && $total_page > 1){
          echo '<a href="index.php?page='.($current_page+1).'">Next</a> | ';
         }
    
       ?>
         <table table class="table table-striped table-dark">
             <thead>
                 <tr>
                     <th>Name</th>
                     <th>Price</th>
                     <th>Description</th>
                     <th colspan="2">Action</th>
                 </tr>
             </thead>
         <?php while($row = $result->fetch_assoc()) :?>
            <tr>
                 <td><?php echo $row['Name'] ;?></td>   
                 <td><?php echo  $row['Price']  ;?></td> 
                 <td><?php echo  $row['Description'] ;?></td> 
                <td>
                     <a href="edit.php?edit=<?php echo $row['ID'];?>"
                     class="btn btn-info"> Edit </a>
                     <a href="connect.php?delete=<?php echo $row[ 'ID']; ?>"
                    class="btn btn-danger" > Delete </a>
                    
                 </td>
             </tr>
         <?php endwhile; ?> 

         </table>   
         
      
       </div>
    
      
     
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 
        
 </body>
 </html>