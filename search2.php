<?php require_once 'connect.php';?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Bakery</title>
    <style>
        .myDiv {
            margin: auto;
            display: flex;
            justify-content: center;
        }
    </style>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body style="background-image: url('https://www.arlafoodsingredients.com/492f6a/globalassets/afi/industry/bakery/bakery-hero.jpg');">

<h1 style="text-align:center;font-size:500%;"> Bakery  </h1>
<body>

<div class="container">

<?php
    if (isset($_POST['submit-search']))?>
    <?php
        
        $search = mysqli_real_escape_string($mysqli, $_POST['search']);
        $sql = "SELECT * FROM bakery WHERE Name LIKE '%$search%' OR Price LIKE '%$search%' OR Description LIKE '%$search%'";
        $result = mysqli_query($mysqli, $sql);
        $queryResult = mysqli_num_rows($result);
        

 ?>  
  <div class="row justify-content-center">
         <table table class="table table-striped table-dark">
            <thead>
              <tr>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Description</th>
                  <th colspan="2">Action</th>
             </tr>
           </thead>
                   

         <?php 
           if ($queryResult > 0 ) {?>
                <?php 
                  while ($row = mysqli_fetch_assoc($result)){ ?>
                   <tr>
                      <td><?php echo $row['Name'] ;?></td>   
                      <td><?php echo  $row['Price']  ;?></td> 
                      <td><?php echo  $row['Description'] ;?></td> 
                      <td>
                         <a href="edit.php?edit=<?php echo $row['ID'];?>"
                         class="btn btn-info"> Edit </a>
                         <a href="connect.php?delete=<?php echo $row[ 'ID']; ?>"
                          class="btn btn-danger" > Delete </a>
                     </td>
                  </tr>
                      <?php  } ?>  
                <?php  } ?>
         
          </table>

        <a href="1234.php"><button class="btn btn-primary" type="back" name="back" >Back</button></a>

</body>
</html>